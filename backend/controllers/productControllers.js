const mongoose = require("mongoose");
const Product = require("../models/product.js");
const auth = require("../auth.js")

//  Create Product
module.exports.addProduct = (reqBody) =>{
  if(reqBody.isAdmin == true){  
    let newProduct = new Product({
    name: reqBody.product.name,
    description: reqBody.product.description,
    price: reqBody.product.price,
    category: reqBody.product.category,
    stocks: reqBody.product.stocks
  })
  
  return newProduct.save().then((newProduct, error)=>
  {
    if(error){
        return error;
    }
    else{
      return newProduct;
    }
  })
}
else{
  let message = Promise.resolve('User must be ADMIN to access this functionality');
  console.log(message)
  return false;
}  
}
// Get all products

module.exports.getAllProducts = (req, res) =>{
	
	const userData = auth.decode(req.headers.authorization);
console.log(userData.isAdmin);
	if(userData.isAdmin){
		return Product.find({}).then(result => res.send(result));
	}
	else{
		return res.send(false);
		// return res.status(401).send("You don't have access to this page!");
	}
}

// Get all active products
module.exports.getActiveProducts = () => {
  return Product.find({isActive: true}).then(result =>{
    return result;
  })
}

// retrieve single product
module.exports.getProduct= (req, res) =>{
	console.log(req.params.productId);

	return Product.findById(req.params.productId).then(result => res.send(result));
}

//  update a product
module.exports.updateProduct = (productId, newData) => {
  if(newData.isAdmin == true){
    return Product.findByIdAndUpdate(productId,
      {
        name: newData.product.name, 
        description: newData.product.description,
        price: newData.product.price,
        category: newData.product.category,
        stocks: newData.product.stocks
      },
      {new: true}
    ).then((result, error)=>{
      if(error){
        return error;
      }
      return result
    }) 
  }
  else{
    let message = Promise.resolve('User must be ADMIN to access this functionality');
    console.log(message);
    return false;
  }
}


//  archive product
module.exports.archiveProduct = (productId, archive) =>{
  if(archive.isAdmin == true){

    return Product.findByIdAndUpdate(productId, 
      {
        isActive : archive.product.isActive
      },
      {
        new : true
      })
    .then((result, error)=>{
      if(error){
          return false;
        }
     else{
      return true
     }
    }
    )
    }
    else{
      let message = Promise.resolve('User must be ADMIN to access this functionality');
      return message.then((value) => {return value});
    }
}

// filter product by name
module.exports.filterProductsByName = async (req, res) => {
  const data = {
    name: req.body.name
  };

  try {
    const filteredProducts = await Product.find({
      name: { $regex: new RegExp(data.name, "i") }
    });

    return res.json( filteredProducts );
  } catch (err) {
    res.send(err);
  }
};

// filter product by category

module.exports.filterProductsByCategory = async (req, res) => {
  const data = {
    category: req.body.category
  };

  try {
    const filteredProducts = await Product.find({
      category: { $regex: new RegExp(data.category, "i") }
    });

    return res.json( filteredProducts );
  } catch (err) {
    res.send(err);
  }
};