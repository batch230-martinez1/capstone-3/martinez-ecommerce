
// Dependencies 
const express= require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const productRoutes = require("./routes/productRoutes.js");
const userRoutes = require("./routes/userRoutes.js");

//  to create a express server/application
const app = express();

//  Middlewares - allows to bridge our backend application (server) to our front end
//  To allow cross origin resource sharing
app.use(cors());
//  to read json objects
app.use(express.json());
// to read forms
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes)
app.use("/products", productRoutes)


// Connect to our MongoDB database

mongoose.connect("mongodb+srv://admin:admin@batch230.s8nzwea.mongodb.net/Martinezecommerce?retryWrites=true&w=majority",{
  useNewUrlParser: true,
  useUnifiedTopology : true
})

mongoose.connection.once("open", () => console.log("Now connected to database"));

app.listen(process.env.PORT || 4000, () =>
  {console.log(`API is now online on port ${process.env.PORT || 4000}`)
});


